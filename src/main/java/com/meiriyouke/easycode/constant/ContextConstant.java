package com.meiriyouke.easycode.constant;

/**
 * velocity context 变量名称
 * User: liyd
 * Date: 13-12-6
 * Time: 下午4:37
 */
public class ContextConstant {

    /** table */
    public static final String TABLE         = "table";

    /** 模板 */
    public static final String TEMPLATE      = "template";

    /** 前缀 */
    public static final String BEGIN_FIX     = "beginFix";

    /** 后缀 */
    public static final String END_FIX       = "endFix";

    /** 扩展名 */
    public static final String SUFFIX        = "suffix";

    /** 目标目录 */
    public static final String TARGET_DIR    = "targetDir";

    /** 包名 */
    public static final String PACKAGE       = "package";

    public static final String TABLE_DESC    = "tableDesc";

    public static final String TABLE_COLUMNS = "tableColumns";
}
